import React from "react"
import data from "./data"
import Card from "./components/Card"
import Hero from "./components/Hero"
import { Navbar } from "./components/Navbar"


const styles ={
  cardList:{
    display: 'flex',
    flexWrap: 'nowrap',
    gap: '20px',
    overflow: 'auto',
  }
}

export default function App() {

  const cards = data.map(item => {
    return (
        <Card 
            key={item.id}
            {...item}
          

        />
    )
  })        

    return (
        <div>
          <Navbar/>
          <Hero/>
          <section style={styles.cardList}>
            {cards}
          </section>
          
        </div>
        
    
        
        
    )
}