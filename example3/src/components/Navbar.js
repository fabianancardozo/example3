import { FaAirbnb } from 'react-icons/fa';

const styles={
    nav:{
      height: '70px',
      display: 'flex',
      padding: '20px 36px',
      boxShadow: '0px 2.98256px 7.4564px rgba(0, 0, 0, 0.1)',
    },
    logo:{
      fontSize: '100px',
      marginTop: '-15px',
      color: '#FF5A5F',
    },
    title:{
        fontFamily: 'Dongle, sans-serif',
        fontSize: '50px',
        marginTop: '10px',

        color: '#FF5A5F',

  
    },
  }

export function Navbar() {
    return(
        <nav style={styles.nav}>
            <FaAirbnb style={styles.logo} />
            <h1 style={styles.title}>airbnb</h1>

        </nav>
    )
    
}

