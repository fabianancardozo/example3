import React from "react"


const styles = {
    section: {
        display: 'flex',
        flexDirection: 'column',
        marginLeft: '10px',
    },
    persons: {
        marginTop: '10px',
        width: '395.91px',
        height: '232px',
        left: '77px',
        top: '96px',
    },
    title: {
        marginBottom: '16px',
        
        fontFamily: 'Poppins',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: '36px',
        lineHeight: '110%',

        color: '#000000',
    },
    text: {
        marginTop: '0',

        width: '320px',
        height: '56px',
        left: '36px',
        top: '416px',

        fontFamily: 'Poppins',
        fontStyle: 'normal',
        fontWeight: '300',
        fontSize: '16px',
        lineHeight: '110%',

        color: '#222222',
    },
}

export default function Hero() {
    return (
        <section style={styles.section}>
            <img src={require('../images/persons.png')} style={styles.persons} />
            <h1 style={styles.title}>Online Experiences</h1>
            <p style={styles.text}>Join unique interactive activities led by one-of-a-kind hosts—all without leaving home.</p>
        </section>
    )
}