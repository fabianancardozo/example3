import React from "react"
import { AiFillStar } from "react-icons/ai";

const styles = {
    card:{
        width: '175px',
        fontSize: '12px',
        flex: '0 0 auto',
        display: 'flex',
        flexDirection: 'column',
        position: 'relative',
    },
    badge:{
        position: 'absolute',
        top: '6px',
        left: '6px',
        backgroundColor: 'white',
        padding: '5px 7px',
        borderRadius: '2px',
        fontWeight: 'bold',
    },
    image: {
        width: '176px',
        height: '235px',
        borderRadius: '9px',
    },
    stats:{
        display: 'flex',
        alignItems: 'center',
    },
    star:{
        height: '14px',
    },
    icon:{
        color: '#FF5A5F',
    },
    gray:{
        color: '#918E9B',

    },
    title:{
        overflow: 'hidden',
        textOverflow: 'ellipsis',
    },
    price:{
        marginTop: 'auto',
    },
    bold:{
        fontWeight: 'bold',
    },
}
export default function Card(props) {

    let badgeText
    if (props.openSpots === 0)  {
        badgeText = "SOLD OUT"
    }else if (props.location === "Online") {
        badgeText = "ONLINE"
    }


    return (
        <div style={styles.card}>
            {badgeText && <div style={styles.badge}>{badgeText}</div>}
            <img src={require(`../../public/images/${props.coverImg}`)} style={styles.image}/>
            <div style={styles.stats}>
                <AiFillStar style={styles.icon}/>
                <span>{props.stats.rating}</span>
                <span style={styles.gray}> ({props.stats.reviewCount}) • </span>
                <span style={styles.gray}>{props.location}</span>
            </div>
                
            <p>{props.title}</p>
            <p style={styles.price}><span style={styles.bold}>From ${props.price}</span> / person</p>
        </div>
    )
}
